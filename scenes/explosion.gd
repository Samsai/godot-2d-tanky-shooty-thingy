extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var lifetime = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	self.get_node("ExplosionSFX").play()
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	lifetime -= delta
	
	if lifetime <= 0.5:
		self.get_node("CPUParticles2D").emitting = false
	elif lifetime <= 0:
		self.queue_free()
	
	pass
