extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var hitpoints = 100
var speed = 100.0
var heading = 0
var moving_forward = false
var moving_backward = false
var movement_sfx_playing = false

var fire_delay = 0
var secondary_fire_delay = 0

var cannon_projectile_scene = preload("res://scenes/projectile.tscn")
var mg_projectile_scene = preload("res://scenes/mg_projectile.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("player")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if self.fire_delay > 0:
		self.fire_delay -= delta
	if self.secondary_fire_delay > 0:
		self.secondary_fire_delay -= delta
	
	if Input.is_action_pressed("turn_left"):
		self.rotate(deg2rad(-180 * delta))
	elif Input.is_action_pressed("turn_right"):
		self.rotate(deg2rad(180 * delta))
	
	if Input.is_action_pressed("throttle"):
		self.moving_forward = true
	else:
		self.moving_forward = false
	
	if Input.is_action_pressed("reverse"):
		self.moving_backward = true
	else:
		self.moving_backward = false
	
	if Input.is_action_pressed("fire") and self.fire_delay <= 0:
		self.fire_delay = 1
		
		var projectile = cannon_projectile_scene.instance()
		projectile.friendly = true
		
		self.get_parent().add_child(projectile)
		
		var projectile_spawn = self.get_node("Turret/ProjectileSpawn").get_global_transform()
		
		projectile.set_global_transform(projectile_spawn)
		
		self.get_node("MainGunSFX").play()
	
	if Input.is_action_pressed("secondary_fire") and self.secondary_fire_delay <= 0:
		self.secondary_fire_delay = 0.1
		
		var projectile = mg_projectile_scene.instance()
		
		self.get_parent().add_child(projectile)
		
		var projectile_spawn = self.get_node("Turret/ProjectileSpawn").get_global_transform()
		
		projectile.set_global_transform(projectile_spawn)
		
		self.get_node("SecondaryGunSFX").play()
	
	# Play the tank audio when we are moving
	if (self.moving_forward or self.moving_backward) and not self.movement_sfx_playing:
		self.get_node("TankMovingSFX").play()
		self.movement_sfx_playing = true
	elif not (self.moving_forward or self.moving_backward) and self.movement_sfx_playing:
		self.movement_sfx_playing = false
	
	# Fading the tank audio in and out
	if self.movement_sfx_playing:
		var sfx = self.get_node("TankMovingSFX")
		
		print("DBs: " + str(sfx.volume_db))
		
		if sfx.volume_db <= 0:
			sfx.volume_db += 160 * delta;
	else:
		var sfx = self.get_node("TankMovingSFX")
		
		if sfx.volume_db > -80:
			sfx.volume_db -= 40 * delta;
		else:
			sfx.stop()
	

func _physics_process(delta):
	var rot = self.rotation
	
	if self.moving_forward:
		var delta_x = sin(rot) * speed * delta
		var delta_y = (-1) * cos(rot) * speed * delta
		
		self.move_and_collide(Vector2(delta_x, delta_y))
	elif self.moving_backward:
		var delta_x = sin(rot) * (-speed) * delta
		var delta_y = (-1) * cos(rot) * (-speed) * delta
		
		self.move_and_collide(Vector2(delta_x, delta_y))
	
	var collision = self.move_and_collide(Vector2(0, 0))
	
	if collision:
		var collider = collision.get_collider()
		
		if collider in self.get_tree().get_nodes_in_group("enemy projectile"):
			self.hitpoints -= collider.get_damage()
			print("hitpoints: " + str(self.hitpoints))
	