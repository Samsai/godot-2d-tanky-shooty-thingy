extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var lifetime = 2
var speed = 500

var damage = 10

var friendly = true

var collided = false

var explosion_scene = preload("res://scenes/explosion.tscn")

func get_damage():
	return self.damage

# Called when the node enters the scene tree for the first time.
func _ready():
	if self.friendly:
		add_to_group("player projectile")
	else:
		add_to_group("enemy projectile")
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.lifetime -= delta;
	
	if self.lifetime <= 0:
		self.queue_free()
	
	pass

func _physics_process(delta):
	if collided:
		
		queue_free()
	
	var rot = self.rotation
	
	var delta_x = sin(rot) * speed * delta
	var delta_y = (-1) * cos(rot) * speed * delta
	
	var collision = self.move_and_collide(Vector2(delta_x, delta_y))
	
	if collision:
		collided = true
	
	pass