extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var lifetime = 2
var speed = 500

var damage = 10

var collided = false

var friendly = true

var explosion_scene = preload("res://scenes/explosion.tscn")

func get_damage():
	return self.damage

# Called when the node enters the scene tree for the first time.
func _ready():
	if self.friendly:
		add_to_group("player projectile")
	else:
		add_to_group("enemy projectile")
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.lifetime -= delta;
	
	if self.lifetime <= 0:
		self.queue_free()
	
	pass

func _physics_process(delta):
	if collided:
		
		queue_free()
	
	var rot = self.rotation
	
	var delta_x = sin(rot) * speed * delta
	var delta_y = (-1) * cos(rot) * speed * delta
	
	var collision = self.move_and_collide(Vector2(delta_x, delta_y))
	
	if collision:
		var collider = collision.get_collider()
		
		if not (collider in self.get_tree().get_nodes_in_group("player projectile") or collider in self.get_tree().get_nodes_in_group("enemy projectile")):
			var explosion = explosion_scene.instance()
		
			self.get_parent().add_child(explosion)
		
			var explosion_spawn = collision.collider.get_global_transform()
		
			explosion.set_global_transform(explosion_spawn)
			collided = true
	
	pass