extends Sprite

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var pointing_to = Vector2(0, 0)

var current_pointing = Vector2(0,0)

var using_mouse = true

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var mouse_pos = get_global_mouse_position()
	var pos = self.get_global_position()
	
	if self.using_mouse:
		self.pointing_to = Vector2(mouse_pos.x - pos.x, mouse_pos.y - pos.y) 
	
	var heading_vector = Vector2(sin(self.rotation + self.get_parent().rotation), -cos(self.rotation + self.get_parent().rotation))
	
	var new_heading = heading_vector.slerp(self.pointing_to, delta)
	
	var rotation = atan2(new_heading.x, -1 * new_heading.y)
	
	self.rotation = rotation - self.get_parent().rotation

func _input(event):
	# Left-right axis: 2
	# Up-down axis: 3
	
	if event is InputEventJoypadMotion:
		self.using_mouse = false
		
		if event.axis_value != 0:
			if event.axis == 2:
				self.pointing_to.x = event.axis_value
			elif event.axis == 3:
				self.pointing_to.y = event.axis_value
	elif event is InputEventMouseMotion:
		self.using_mouse = true
	