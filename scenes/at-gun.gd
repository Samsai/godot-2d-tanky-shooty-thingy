extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var targeting_los = self.get_node("targeting_los")

var firing_cooldown = 3.0
var hitpoints = 10

var projectile_scene = preload("res://scenes/projectile.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if self.hitpoints <= 0:
		self.queue_free()
	
	if self.firing_cooldown >= 0:
		self.firing_cooldown -= delta
	
	if self.firing_cooldown < 0:
		self.targeting_los.enabled = true
		if self.targeting_los.is_colliding():
			var collider = self.targeting_los.get_collider()
		
			if collider in self.get_tree().get_nodes_in_group("player"):
				self.firing_cooldown = 3.0
				
				var projectile = projectile_scene.instance()
				projectile.friendly = false
		
				self.get_parent().add_child(projectile)
		
				var projectile_spawn = self.get_node("projectile_spawn").get_global_transform()
		
				projectile.set_global_transform(projectile_spawn)
				
				self.get_node("GunShotSFX").play()
				
				self.targeting_los.enabled = false
				

func _physics_process(delta):
	var player = self.get_tree().get_nodes_in_group("player")[0]
	var player_pos = player.get_global_position()
	
	var pos = self.get_global_position()
	
	var relative_pos = Vector2(player_pos.x - pos.x, player_pos.y - pos.y) 
	
	var heading_vector = Vector2(sin(self.rotation), -cos(self.rotation))
	
	var new_heading = heading_vector.slerp(relative_pos, delta)
	
	var AP = (Vector2(player_pos.x, player_pos.y) - Vector2(pos.x, pos.y)).normalized()
	
	if AP.dot(heading_vector) > 0:
		var rotation = atan2(new_heading.x, -1 * new_heading.y)
		self.rotation = rotation - self.get_parent().rotation
	
	# Collision stuff
	
	var collision = self.move_and_collide(Vector2(0, 0))
	
	if collision:
		var collider = collision.get_collider()
		
		if collider in self.get_tree().get_nodes_in_group("player projectile"):
			self.hitpoints -= collider.get_damage()
