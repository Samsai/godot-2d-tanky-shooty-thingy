extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _physics_process(delta):
	var collision = self.move_and_collide(Vector2(0,0))
	
	if collision:
		var collider = collision.get_collider()
		
		var player_projectiles = get_tree().get_nodes_in_group("player projectile")
		
		if collider in player_projectiles:
			print("BOOM")
			
			self.queue_free()
	
	pass